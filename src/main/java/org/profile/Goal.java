package org.profile;

public enum Goal {
    LOSE_WEIGHT,
    GAIN_WEIGHT,
    MAINTAIN_WEIGHT,
    GAIN_MUSCLE
}
