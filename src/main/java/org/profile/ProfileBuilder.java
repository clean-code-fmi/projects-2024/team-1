package org.profile;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public final class ProfileBuilder {
    private Scanner scanner;
    private Set<BarriersWeightLoss> selectedBarriersWeightLoss;
    private Set<BarriersWeightMaintenance> selectedBarriersWeightMaintenance;
    private Set<ReasonsGainWeight> selectedReasonsWeightGain;
    private ResultsMuscleGain resultMuscleGain;
    private UserProfile userProfile;
    public ProfileBuilder() {
        this.scanner = new Scanner(System.in);
        this.selectedBarriersWeightLoss = new HashSet<>();
        this.selectedBarriersWeightMaintenance = new HashSet<>();
        this.selectedReasonsWeightGain = new HashSet<>();
        this.userProfile = new UserProfile();
    }
    public void setGoals(Set<Goal> goals) {
        userProfile.setGoals(goals);
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public Set<BarriersWeightLoss> getSelectedBarriersWeightLoss() {
        return selectedBarriersWeightLoss;
    }

    public Set<BarriersWeightMaintenance> getBarriersWeightMaintenance() {
        return selectedBarriersWeightMaintenance;
    }

    public Set<BarriersWeightMaintenance> getSelectedBarriersWeightMaintenance() {
        return selectedBarriersWeightMaintenance;
    }

    public Set<ReasonsGainWeight> getSelectedReasonsWeightGain() {
        return selectedReasonsWeightGain;
    }

    public ResultsMuscleGain getResultMuscleGain() {
        return resultMuscleGain;
    }
    public UserProfile getUserProfile() {
        return userProfile;
    }
    private void getHeightInformation() {
        int choice;
        System.out.println("Enter 1 for height in cm, "
                + "2 for height in feet/inches");
        do {
            choice = scanner.nextInt();
        } while (choice != 1 && choice != 2);
        System.out.println("How tall are you?");
        if (choice == 1) {
            userProfile.setHeightMeasurement("cm");
            int height;
            height = scanner.nextInt();
            userProfile.setHeight(String.valueOf(height));
        } else {
            userProfile.setHeightMeasurement("ft/in");
            System.out.println("Enter feet:");
            int ft = scanner.nextInt();
            System.out.println("Enter inches:");
            int in = scanner.nextInt();
            userProfile.setHeight(String.format("%d ft %d in", ft, in));
        }
    }
    private void getWeightInformation(int option) {
        int choice;
        if (option == 1) {
            System.out.println("Enter 1 for weight in pounds,"
                    + " 2 for weight in kilograms, 3 for weight in stone:");
            do {
                choice = scanner.nextInt();
            } while (choice != 1 && choice != 2 && choice != 3);
            System.out.println("How much do you weigh?");
            if (choice == 1) {
                userProfile.setWeightMeasurement("pounds");
                int weight;
                weight = scanner.nextInt();
                userProfile.setWeight(String.valueOf(weight));
            } else if (choice == 2) {
                userProfile.setWeightMeasurement("kg");
                int weight = scanner.nextInt();
                userProfile.setWeight(String.valueOf(weight));
            } else {
                userProfile.setWeightMeasurement("stone");
                System.out.println("Enter stone:");
                int st = scanner.nextInt();
                System.out.println("Enter lbs:");
                int lbs = scanner.nextInt();
                userProfile.setWeight(String.format("%d st %d lbs", st, lbs));
            }
        } else {
            System.out.println("What's your goal weight?");
            if (userProfile.getWeightMeasurement().equals("pounds")) {
                int weightGoal;
                weightGoal = scanner.nextInt();
                userProfile.setWeightGoal(String.valueOf(weightGoal));
            } else if (userProfile.getWeightMeasurement().equals("kg")) {
                int weightGoal = scanner.nextInt();
                userProfile.setWeightGoal(String.valueOf(weightGoal));
            } else {
                System.out.println("Enter stone:");
                int st = scanner.nextInt();
                System.out.println("Enter lbs:");
                int lbs = scanner.nextInt();
                userProfile.setWeightGoal(String.format("%d st %d lbs", st, lbs));
            }
        }
    }
    private void askAdditionalQuestionsLoseWeight() {
        int choice;
        int counter = 1;
        System.out.println(
                "In the past, what were barriers"
                        + " to achieving weight loss?");
        System.out.println("Select by number all that apply:");
        for (BarriersWeightLoss barrier
                : BarriersWeightLoss.values()) {
            System.out.printf(counter + ". " + barrier + "\n");
            counter++;
        }
        do {
            System.out.println("Enter your choice (0 to finish):");
            choice = scanner.nextInt();
            if (choice > 0 && choice
                    <= BarriersWeightLoss.values().length) {
                selectedBarriersWeightLoss.add(
                        BarriersWeightLoss.values()[choice - 1]);
            }
        } while (choice != 0);
    }
    private void askAdditionalQuestionsGainWeight() {
        int choice;
        int counter = 1;
        System.out.println(
                "What are your reasons "
                        + "for wanting to gain weight?");
        for (ReasonsGainWeight reason
                : ReasonsGainWeight.values()) {
            System.out.printf(counter + ". " + reason + "\n");
            counter++;
        }
        do {
            System.out.println("Enter your choice (0 to finish):");
            choice = scanner.nextInt();
            if (choice > 0 && choice
                    <= ReasonsGainWeight.values().length) {
                selectedReasonsWeightGain.add(
                        ReasonsGainWeight.values()[choice - 1]);
            }
        } while (choice != 0);
    }
    private void askAdditionalQuestionsMaintainWeight() {
        int choice;
        int counter = 1;
        System.out.println(
                "In the past, what were "
                        + "barriers to maintaining weight?");
        System.out.println("Select by number all that apply:");
        for (BarriersWeightMaintenance barrier
                : BarriersWeightMaintenance.values()) {
            System.out.printf(counter + ". " + barrier + "\n");
            counter++;
        }
        do {
            System.out.println("Enter your choice (0 to finish):");
            choice = scanner.nextInt();
            if (choice > 0 && choice
                    <= BarriersWeightMaintenance.values().length) {
                selectedBarriersWeightMaintenance.add(
                        BarriersWeightMaintenance.values()[choice - 1]);
            }
        } while (choice != 0);
    }
    private void askAdditionalQuestionsGainMuscle() {
        int choice;
        int counter = 1;
        System.out.println(
                "What results do you want to achieve from gaining muscle?");
        System.out.println("Select one by number:");
        for (ResultsMuscleGain result : ResultsMuscleGain.values()) {
            if (result.equals(ResultsMuscleGain.TONE_UP)) {
                System.out.printf(counter + ". " + result
                        + " - you want visible muscles with as little mass as possible, "
                        + "with a low body fat percentage\n");
            } else if (result.equals(ResultsMuscleGain.BULK_UP)) {
                System.out.printf(counter + ". " + result
                        + " - you want large, well-defined muscles, "
                        + "with a low percentage of body fat\n");
            } else if (result.equals(ResultsMuscleGain.GET_STRONG)) {
                System.out.printf(counter + ". " + result
                        + " - you want to lift the maximum amount of weight "
                        + "and are not concerned with body fat or muscle definition\n");
            }
            counter++;
        }
        do {
            System.out.println("Enter your choice:");
            choice = scanner.nextInt();
            if (choice > 0 && choice <= ResultsMuscleGain.values().length) {
                resultMuscleGain = ResultsMuscleGain.values()[choice - 1];
                break;
            } else {
                System.out.println("Invalid choice! Please choose again.");
            }
        } while (choice != 0);
    }
    public void askAdditionalQuestionsBasedOnGoal() {
        for (Goal goal : userProfile.getGoals()) {
            switch (goal) {
                case LOSE_WEIGHT:
                    askAdditionalQuestionsLoseWeight();
                    break;
                case GAIN_WEIGHT:
                   askAdditionalQuestionsGainWeight();
                    break;
                case MAINTAIN_WEIGHT:
                    askAdditionalQuestionsMaintainWeight();
                    break;
                case GAIN_MUSCLE:
                    askAdditionalQuestionsGainMuscle();
                    break;
                default:
                    System.out.println("Invalid choice. Please choose again.");
                    break;
            }
        }
    }

    public void askBaselineActivityLevel() {
        System.out.println("What is your baseline activity level?");
        System.out.println("Not including workouts - we count that separately.");
        for (ActivityLevel level : ActivityLevel.values()) {
            int counter = 1;
            if (level.equals(ActivityLevel.NOT_VERY_ACTIVE)) {
                System.out.printf(counter + ". " + level
                        + "\nSpend most of the day sitting (e.g. bankteller, desk job\n");
            } else if (level.equals(ActivityLevel.LIGHTLY_ACTIVE)) {
                System.out.printf(counter + ". " + level
                        + "\nSpend a good part of the day on your feet (e.g. teacher, salesperson\n");
            } else if (level.equals(ActivityLevel.ACTIVE)) {
                System.out.printf(counter + ". " + level
                        + "\nSpend a good part of the day doing some physical activity "
                        + "(e.g. food server, postal carrier\n");
            } else if (level.equals(ActivityLevel.VERY_ACTIVE)) {
                System.out.printf(counter + ". " + level
                        + "\nSpend most of the day doing heavy physical activity (e.g. bike messenger, carpenter\n");
            }
        }
        int choice;
        do {
            System.out.println("Enter your choice:");
            choice = scanner.nextInt();
            if (choice > 0 && choice <= ActivityLevel.values().length) {
                userProfile.setActivityLevel(ActivityLevel.values()[choice - 1]);
                break;
            } else {
                System.out.println("Invalid choice! Please choose again.");
            }

        } while (choice != 0);
    }

    private void askForGender() {
        System.out.println(
                "Please select which sex we should use to calculate your calorie needs:");
        int counter = 1;
        for (Gender gender : Gender.values()) {
            System.out.printf(counter + ". " + gender + "\n");
            counter++;
        }
        int choice;
        do {
            System.out.println("Enter your choice:");
            choice = scanner.nextInt();
            if (choice > 0 && choice <= Gender.values().length) {
                userProfile.setGender(Gender.values()[choice - 1]);
                break;
            } else {
                System.out.println("Invalid choice! Please choose again.");
            }

        } while (choice != 0);
    }
    public void askPersonalInfo() {
        System.out.println("Tell us a little bit about yourself");
        askForGender();
        System.out.println("How old are you?");
        int age;
        age = scanner.nextInt();
        userProfile.setAge(age);
        System.out.println("Where do you live?");
        String country;
        country = scanner.next();
        userProfile.setCountry(country);
        System.out.println("Just a few more questions");
        getHeightInformation();
        getWeightInformation(1);
        for (Goal goal : userProfile.getGoals()) {
            if (goal.equals(Goal.GAIN_WEIGHT) || goal.equals(Goal.LOSE_WEIGHT)) {
                getWeightInformation(2);
            }
        }
    }
    public void createAccount() {
        System.out.println("Almost done! Create your account.");
        System.out.println("Enter your email:");
        String email;
        email = scanner.next();
        userProfile.setEmail(email);
        String pw;
        do {
            System.out.println("Enter your password (10 characters minimum): ");
            pw = scanner.next();
        } while (pw.length() < 10);
        userProfile.setPassword(pw);
    }
}

