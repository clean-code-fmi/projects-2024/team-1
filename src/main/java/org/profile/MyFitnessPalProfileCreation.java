package org.profile;

public final class MyFitnessPalProfileCreation {
    private MyFitnessPalProfileCreation() {
    }
    public static void main(String[] args) {
        ProfileCreationController controller = new ProfileCreationController();
        controller.startProfileCreation();
    }
}

