package org.profile;

public enum ActivityLevel {
    NOT_VERY_ACTIVE,
    LIGHTLY_ACTIVE,
    ACTIVE,
    VERY_ACTIVE
}

