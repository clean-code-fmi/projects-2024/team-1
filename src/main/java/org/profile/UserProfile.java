package org.profile;

import java.util.HashSet;
import java.util.Set;

public final class UserProfile {
    private Set<Goal> goals;
    private String weight;
    private String weightGoal;
    private String height;
    private int age;
    private String country;
    private Gender gender;
    private String email;
    private String password;
    private ActivityLevel activityLevel;
    private String heightMeasurement;
    private String weightMeasurement;

    public UserProfile() {
        this.goals = new HashSet<>();
    }
    public void setGoals(Set<Goal> goals) {
        this.goals.addAll(goals);
    }

    public Set<Goal> getGoals() {
        return goals;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeightGoal() {
        return weightGoal;
    }

    public void setWeightGoal(String weightGoal) {
        this.weightGoal = weightGoal;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public ActivityLevel getActivityLevel() {
        return activityLevel;
    }

    public String getHeightMeasurement() {
        return heightMeasurement;
    }

    public String getWeightMeasurement() {
        return weightMeasurement;
    }
    public void setActivityLevel(ActivityLevel activityLevel) {
        this.activityLevel = activityLevel;
    }

    public void setHeightMeasurement(String heightMeasurement) {
        this.heightMeasurement = heightMeasurement;
    }

    public void setWeightMeasurement(String weightMeasurement) {
        this.weightMeasurement = weightMeasurement;
    }
}
