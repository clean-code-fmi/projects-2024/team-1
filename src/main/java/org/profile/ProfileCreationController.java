package org.profile;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public final class ProfileCreationController {
    private Scanner scanner;
    private ProfileBuilder profileBuilder;

    public ProfileCreationController() {
        this.scanner = new Scanner(System.in);
        this.profileBuilder = new ProfileBuilder();
    }
    public ProfileCreationController(ProfileBuilder profileBuilder, Scanner scanner) {
        this.profileBuilder = profileBuilder;
        this.scanner = scanner;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public ProfileBuilder getProfileBuilder() {
        return profileBuilder;
    }

    public void setProfileBuilder(ProfileBuilder profileBuilder) {
        this.profileBuilder = profileBuilder;
    }

    public void startProfileCreation() {
        askForGoal();

        profileBuilder.askAdditionalQuestionsBasedOnGoal();

        profileBuilder.askBaselineActivityLevel();

        profileBuilder.askPersonalInfo();

        profileBuilder.createAccount();

        System.out.println("Your MyFitnessPal profile has been created successfully!");
    }

    public void askForGoal() {
        System.out.println("You can choose up to 2 goals or less:");
        for (Goal goal : Goal.values()) {
            System.out.printf("%d. %s\n", goal.ordinal() + 1, goal.toString());
        }

        Set<Goal> choices = new HashSet<>();
        int choice;
        do {
            System.out.println("Choose your goals (enter the number, 0 to finish):");
            choice = scanner.nextInt();
            if (choice > 0 && choice <= Goal.values().length) {
                if (!choices.isEmpty()) {
                    if ((choice == 3 && (choices.contains(Goal.LOSE_WEIGHT)
                            || choices.contains(Goal.GAIN_WEIGHT))
                            || (choice == 2 && (choices.contains(Goal.LOSE_WEIGHT)
                            || choices.contains(Goal.MAINTAIN_WEIGHT)))
                            || (choice == 1 && (choices.contains(Goal.GAIN_WEIGHT)
                            || choices.contains(Goal.MAINTAIN_WEIGHT))))) {
                        continue;
                    } else {
                        choices.add(Goal.values()[choice - 1]);
                    }
                } else {
                    choices.add(Goal.values()[choice - 1]);
                }
            }
        } while (choice != 0 && choices.size() <= 2);

        profileBuilder.setGoals(choices);
    }
}
