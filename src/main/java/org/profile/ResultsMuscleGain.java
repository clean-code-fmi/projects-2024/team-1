package org.profile;

public enum ResultsMuscleGain {
    TONE_UP,
    BULK_UP,
    GET_STRONG
}
