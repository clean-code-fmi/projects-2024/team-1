package org.meal;

import org.food.Food;

import java.util.HashMap;
import java.util.Map;

public final class Meal {
    private String mealName;

    // food - the actual item, double - the quantity
    private Map<Food, Double> mealItems;
    private String mealInstructions;
    private int mealCalories;
    private double mealCarbs;
    private double mealFat;
    private double mealProtein;

    Meal() {
        mealItems = new HashMap<>();
        mealCalories = 0;
        mealCarbs = 0;
        mealProtein = 0;
        mealFat = 0;
    }
    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public Map<Food, Double> getMealItems() {
        return mealItems;
    }

    public void setMealItems(Map<Food, Double> mealItems) {
        this.mealItems.putAll(mealItems);
    }
    public void addMealItem(Food food, double quantity) {
        this.mealItems.put(food, quantity);
    }

    public String getMealInstructions() {
        return mealInstructions;
    }

    public void setMealInstructions(String mealInstructions) {
        this.mealInstructions = mealInstructions;
    }

    public int getMealCalories() {
        return mealCalories;
    }

    public void setMealCalories(int mealCalories) {
        this.mealCalories = mealCalories;
    }

    public double getMealCarbs() {
        return mealCarbs;
    }

    public void setMealCarbs(double mealCarbs) {
        this.mealCarbs = mealCarbs;
    }

    public double getMealFat() {
        return mealFat;
    }

    public void setMealFat(double mealFat) {
        this.mealFat = mealFat;
    }

    public double getMealProtein() {
        return mealProtein;
    }

    public void setMealProtein(double mealProtein) {
        this.mealProtein = mealProtein;
    }
    public void setMacros() {
        for (Map.Entry<Food, Double> entry : mealItems.entrySet()) {
            double quantity = entry.getValue();

            mealCalories += entry.getKey().getCalories() * (quantity / 100); // assuming calories per 100 grams
            mealProtein += entry.getKey().getProtein() * (quantity / 100);
            mealCarbs += entry.getKey().getCarbohydrates() * (quantity / 100);
            mealFat += entry.getKey().getFat() * (quantity / 100);
        }
    }
}
