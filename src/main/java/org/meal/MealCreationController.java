package org.meal;

import org.food.Food;

import java.util.Set;
import java.util.HashSet;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public final class MealCreationController {
    private Meal meal;
    private Set<Food> availableFood;
    private Scanner scanner;
    MealCreationController() {
        meal = new Meal();
        availableFood = new HashSet<>();
        scanner = new Scanner(System.in);
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public Set<Food> getAvailableFood() {
        return availableFood;
    }

    public void setAvailableFood(Set<Food> availableFood) {
        this.availableFood.addAll(availableFood);
    }
    public void addFoodToAvailableFood(Food food) {
        this.availableFood.add(food);
    }
    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }
    private void printAvailableFood() {
        int index = 1;
        for (Food food : availableFood) {
            System.out.printf(index + ". " + food.getName() + "\n"
                    + food.getCalories() + "\n"
                    + food.getProtein() + "\n"
                    + food.getCarbohydrates() + "\n"
                    + food.getFat() + "\n"
                    + food.getSugar() + "\n");
            index++;
        }
    }
    private void selectFoodForMeal() {
        int input;
        do {
            System.out.print("Select a food item (enter the corresponding number, 0 to finish): ");
            input = scanner.nextInt();
            try {
                if (input < 1 || input > availableFood.size()) {
                    System.out.println("Invalid selection. Please try again.");
                    continue;
                }
                List<Food> food = new ArrayList<>(availableFood);
                Food selectedFood = food.get(input - 1);
                System.out.print("Enter quantity (in grams): ");
                double quantity = scanner.nextDouble();
                scanner.nextLine();

                meal.addMealItem(selectedFood, quantity);
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please try again.");
            }
        } while (input != 0);
    }
    private void createFood() {
        Food food = new Food();
        food.initializeFood();
        availableFood.add(food);
        System.out.print("Enter quantity (in grams): ");
        double quantity = scanner.nextDouble();
        scanner.nextLine();

        meal.addMealItem(food, quantity);
    }
    private void selectOption() {
        System.out.println("Enter 1 to choose from the options, enter 2 to create new food:");
        int input = scanner.nextInt();
        if (input == 1) {
            selectFoodForMeal();
        } else {
            createFood();
        }
    }
    public void createMeal() {
        System.out.print("Enter the name of your custom meal: ");
        String mealName = scanner.nextLine();
        meal.setMealName(mealName);
        printAvailableFood();
        selectOption();
        meal.setMacros();
    }
}
