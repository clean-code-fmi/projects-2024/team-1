package org.water;

import java.util.Scanner;

public final class WaterIntakeTracker {
    private static double dailyWaterIntakeTotal;
    private static String unit;
    private static Scanner scanner;
    static {
        dailyWaterIntakeTotal = 0;
        unit = "";
        scanner = new Scanner(System.in);
    }
    private WaterIntakeTracker() {
        // Private constructor to prevent instantiation
        unit = "";
        dailyWaterIntakeTotal = 0;
    }
    public static double getDailyWaterIntakeTotal() {
        return dailyWaterIntakeTotal;
    }

    public static void setScanner(Scanner scanner) {
        WaterIntakeTracker.scanner = scanner;
    }

    public static String getUnit() {
        return unit;
    }
    public static void addWater() {
        System.out.println("Enter 1 to change the unit to fluid ounces(oz), "
                + "2 to change the unit to millilitres(ml), "
                + "3 to change the unit to cups: ");
        int choice;
        do {
            choice = scanner.nextInt();
        } while (choice != 1 && choice != 2 && choice != 3);
        double amount;
        if (choice == 1) {
            unit = "oz";
            System.out.println("Enter the amount of water you want to add on your daily intake in oz:");
            System.out.println("OR");
            System.out.printf("Enter 1 to add +8 oz\t Enter 2 to add +17 oz\t Enter 3 to add +24 oz:\n");
            amount = scanner.nextDouble();
            if (amount == 1) {
                dailyWaterIntakeTotal += 8;
            } else if (amount == 2) {
                dailyWaterIntakeTotal += 17;
            } else if (amount == 3) {
                dailyWaterIntakeTotal += 24;
            } else {
                dailyWaterIntakeTotal += amount;
            }
        } else if (choice == 2) {
            unit = "ml";
            System.out.println("Enter the amount of water you want to add on your daily intake in ml:");
            System.out.println("OR");
            System.out.printf("Enter 1 to add +250 ml\t Enter 2 to add +500 ml\t Enter 3 to add +1000 ml:\n");
            amount = scanner.nextDouble();
            if (amount == 1) {
                dailyWaterIntakeTotal += 250;
            } else if (amount == 2) {
                dailyWaterIntakeTotal += 500;
            } else if (amount == 3) {
                dailyWaterIntakeTotal += 1000;
            } else {
                dailyWaterIntakeTotal += amount;
            }
        } else {
            unit = "cups";
            System.out.println("Enter the amount of water you want to add on your daily intake in cups:");
            System.out.println("OR");
            System.out.printf("Enter 1 to add +1 cup\t Enter 2 to add +2 cups\t Enter 3 to add +4 cups:\n");
            amount = scanner.nextDouble();
            if (amount == 1) {
                dailyWaterIntakeTotal += 1;
            } else if (amount == 2) {
                dailyWaterIntakeTotal += 2;
            } else if (amount == 3) {
                dailyWaterIntakeTotal += 4;
            } else {
                dailyWaterIntakeTotal += amount;
            }
        }
    }

}
