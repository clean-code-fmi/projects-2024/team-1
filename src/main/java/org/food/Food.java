package org.food;

import java.util.Scanner;

public final class Food {
    private String nameOfFood;
    private int calories;
    private int protein;
    private int carbohydrates;
    private int sugar;
    private int fat;
    private int portion;
    private Scanner scanner;

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public Food() {
        this.scanner = new Scanner(System.in);
        nameOfFood = "unknown";
        calories = 0;
        protein = 0;
        carbohydrates = 0;
        sugar = 0;
        fat = 0;
        portion = 0;
    }

    public void initializeFood() {
        System.out.println("Enter name of food ");
        String name = scanner.nextLine();
        setName(name);
        System.out.println("Enter calories of food per portion ");
        int intTaken = scanner.nextInt();
        setCalories(intTaken);
        System.out.println("Enter proteins of food per portion ");
        intTaken = scanner.nextInt();
        setProtein(intTaken);
        System.out.println("Enter carbohydrates of food per portion ");
        intTaken = scanner.nextInt();
        setCarbohydrates(intTaken);
        System.out.println("Enter sugars of food per portion ");
        intTaken = scanner.nextInt();
        setSugar(intTaken);
        System.out.println("Enter how many grams are per one portion ");
        intTaken = scanner.nextInt();
        setPortion(intTaken);
    }

    public void setName(String name) {
        nameOfFood = name;
    }

    public String getName() {
        return nameOfFood;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public int getCalories() {
        return calories;
    }

    public void setProtein(int protein) {
        this.protein = protein;
    }

    public int getProtein() {
        return protein;
    }

    public void setCarbohydrates(int carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public int getCarbohydrates() {
        return carbohydrates;
    }

    public void setSugar(int sugar) {
        this.sugar = sugar;
    }

    public int getSugar() {
        return sugar;
    }

    public void setFat(int fat) {
        this.fat = fat;
    }

    public int getFat() {
        return fat;
    }

    public void setPortion(int portion) {
        this.portion = portion;
    }

    public int getPortion() {
        return portion;
    }
}
