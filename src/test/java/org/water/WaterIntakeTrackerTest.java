package org.water;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;


public final class WaterIntakeTrackerTest {

    @Test
    public void testAddWaterInCups() {
        InputStream inputStream = new ByteArrayInputStream("3\n1\n".getBytes());
        Scanner scanner = new Scanner(inputStream);
        WaterIntakeTracker.setScanner(scanner);

        WaterIntakeTracker.addWater();

        assertEquals("cups", WaterIntakeTracker.getUnit());
        assertEquals(1, WaterIntakeTracker.getDailyWaterIntakeTotal());
    }
    @Test
    public void testAddWaterInMl() {
        InputStream inputStream = new ByteArrayInputStream("2\n250\n".getBytes());
        Scanner scanner = new Scanner(inputStream);
        WaterIntakeTracker.setScanner(scanner);

        WaterIntakeTracker.addWater();

        assertEquals("ml", WaterIntakeTracker.getUnit());
        assertEquals(251, WaterIntakeTracker.getDailyWaterIntakeTotal());
    }
    @Test
    public void testAddWaterInOz() {
        // Input 3 to add +24oz to dailyWaterIntake
        InputStream inputStream = new ByteArrayInputStream("1\n3\n".getBytes());
        Scanner scanner = new Scanner(inputStream);
        WaterIntakeTracker.setScanner(scanner);

        WaterIntakeTracker.addWater();

        assertEquals("oz", WaterIntakeTracker.getUnit());
        assertEquals(275, WaterIntakeTracker.getDailyWaterIntakeTotal());
    }
}
