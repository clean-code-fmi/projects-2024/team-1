package org.profile;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public final class ProfileCreationTest {
    private ProfileCreationController profileCreationController;
    private ProfileBuilder profileBuilder;

    @BeforeEach
    public void setUp() {
        profileCreationController = new ProfileCreationController();
        profileBuilder = new ProfileBuilder();
    }

    @Test
    public void testAskForGoalValidInput() {
        // Simulate user input: 1, 2, 3 (valid choices), 0 to finish
        InputStream inputStream = new ByteArrayInputStream("1\n4\n0\n".getBytes());
        Scanner scanner = new Scanner(inputStream);

        // Set the scanner of the ProfileCreationController to the one we created
        profileCreationController.setScanner(scanner);

        profileCreationController.askForGoal();

        assertEquals(2, profileCreationController.getProfileBuilder().getUserProfile().getGoals().size());
    }

    @Test
    public void testAskForGoalInvalidInput() {
        // Simulate user input: 5 (invalid choice), 1, 2, 0 to finish
        InputStream inputStream = new ByteArrayInputStream("5\n1\n2\n0\n".getBytes());
        Scanner scanner = new Scanner(inputStream);

        // Set the scanner of the ProfileCreationController to the one we created
        profileCreationController.setScanner(scanner);

        profileCreationController.askForGoal();

        // Ensure that only valid choices are accepted
        assertEquals(1,  profileCreationController.getProfileBuilder().getUserProfile().getGoals().size());
    }

    @Test
    public void testAskQuestionsBasedOnGoal() {
        Set<Goal> simulatedGoals = new HashSet<>();
        simulatedGoals.add(Goal.GAIN_WEIGHT);
        profileBuilder.setGoals(simulatedGoals);

        InputStream inputStream = new ByteArrayInputStream("1\n3\n0\n".getBytes());
        Scanner scanner = new Scanner(inputStream);
        profileBuilder.setScanner(scanner);
        profileBuilder.askAdditionalQuestionsBasedOnGoal();

        assertEquals(2, profileBuilder.getSelectedReasonsWeightGain().size());

    }

    @Test
    public void testAskBaselineActivityLevel() {
        InputStream inputStream = new ByteArrayInputStream("3\n".getBytes());
        Scanner scanner = new Scanner(inputStream);
        profileBuilder.setScanner(scanner);

        profileBuilder.askBaselineActivityLevel();

        assertEquals(ActivityLevel.ACTIVE, profileBuilder.getUserProfile().getActivityLevel());
    }

    @Test
    public void testAskPersonalInfo() {
        InputStream inputStream = new ByteArrayInputStream("2\n22\nBulgaria\n1\n158\n2\n51\n49".getBytes());
        Scanner scanner = new Scanner(inputStream);
        profileBuilder.setScanner(scanner);

        Set<Goal> simulatedGoals = new HashSet<>();
        simulatedGoals.add(Goal.LOSE_WEIGHT);
        profileBuilder.setGoals(simulatedGoals);

        profileBuilder.askPersonalInfo();

        assertEquals(Gender.FEMALE, profileBuilder.getUserProfile().getGender());
        assertEquals(22, profileBuilder.getUserProfile().getAge());
        assertEquals("Bulgaria", profileBuilder.getUserProfile().getCountry());
        assertEquals("158", profileBuilder.getUserProfile().getHeight());
        assertEquals("51", profileBuilder.getUserProfile().getWeight());
        assertEquals("49", profileBuilder.getUserProfile().getWeightGoal());
    }

    @Test
    public void testCreateAccount() {
        InputStream inputStream = new ByteArrayInputStream("frosina@gmail.com\nfrosinaaaaa".getBytes());
        Scanner scanner = new Scanner(inputStream);
        profileBuilder.setScanner(scanner);

        profileBuilder.createAccount();

        assertEquals("frosina@gmail.com", profileBuilder.getUserProfile().getEmail());
    }
}
